<?php

namespace App\Api\V1\Controllers;

use JWTAuth;
use App\Posts;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use App\Api\V1\Requests\PostsRequest;
use App\Http\Controllers\Controller;
use App\Api\V1\Transformers\PostTransformer;

class PostController extends Controller
{
  use Helpers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $currentUser = JWTAuth::parseToken()->authenticate();

      return $currentUser
          ->posts()
          ->with('comments','replies')
          ->orderBy('created_at', 'DESC')
          ->paginate(25)
          ->toArray();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostsRequest $request)
    {
      $currentUser = JWTAuth::parseToken()->authenticate();

      $posts = new Posts;

      $posts->user_id = $request->get('user_id');
      $posts->title = $request->get('title');
      $posts->slug = $request->get('slug');
      $posts->content = $request->get('content');

      // Save post attach currrent user with it
      if($currentUser->posts()->save($posts))
          return $this->response->item($posts, new PostTransformer);
      else
          return $this->response->error('Unable to create user posts.', 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $currentUser = JWTAuth::parseToken()->authenticate();

      $posts = $currentUser->posts()->find($id);

      if(!$posts)
        return $this->response->error('The given data was invalid.', 500);

      return $this->response->item($posts, new PostTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $currentUser = JWTAuth::parseToken()->authenticate();

      $posts = $currentUser->posts()->find($id);

      $posts->user_id = $request->get('user_id');
      $posts->title = $request->get('title');
      $posts->slug = $request->get('slug');
      $posts->content = $request->get('content');

      // Save post attach currrent user with it
      if($currentUser->posts()->save($posts))
          return $this->response->item($posts, new PostTransformer);
      else
          return $this->response->error('The given data was invalid.', 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $currentUser = JWTAuth::parseToken()->authenticate();

      $posts = $currentUser->posts()->find($id);

      if(!$posts)
        return $this->response->error('The given data was invalid.', 404);

      if($posts->delete())
          return $this->response->array(['status' => 'Record successfully deleted.'], 200);
      else
          return $this->response->error('The given data was invalid.', 500);
    }
}
