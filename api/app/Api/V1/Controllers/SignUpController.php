<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SignUpController extends Controller
{
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {
        $user = new User($request->all());
        if(!$user->save()) {
            throw new HttpException(500);
        }

        $token = $JWTAuth->fromUser($user);

        if(!Config::get('boilerplate.sign_up.release_token')) {
          return response()->json([
              'token' => $token,
              'token_type' => 'bearer',
              'expires_at' => now()
          ], 201);
        }

        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_at' => now()
        ], 201);
    }
}
