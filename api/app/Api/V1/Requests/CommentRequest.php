<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class CommentRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('boilerplate.comments.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
