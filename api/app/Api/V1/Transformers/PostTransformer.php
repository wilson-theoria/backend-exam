<?php

namespace App\Api\V1\Transformers;

use App\Posts;
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
    public function transform(Posts $post)
    {
      // Specify what elements are going to be visible to the API
      return [
          'id' => (int) $post->id,
          'user_id' => (int) $post->user_id,
          'title' => $post->title,
          'slug' => $post->slug,
          'created_at' => $post->created_at->format('Y-m-d H:i:s'),
          'updated_at' => $post->updated_at->format('Y-m-d :i:s')
      ];
    }
}
