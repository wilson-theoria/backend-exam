<?php

namespace App\Api\V1\Transformers;

use App\Comments;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    public function transform(Comments $comment)
    {
      // Specify what elements are going to be visible to the API
      return [
          'id' => (int) $comment->id,
          'body' => $comment->body,
          'commentable_type' => $comment->commentable_type,
          'commentable_id' => $comment->commentable_id,
          'creator_id' => $comment->creator_id,
          'parent_id' => $comment->parent_id,
          'created_at' => $comment->created_at->format('Y-m-d H:i:s'),
          'updated_at' => $comment->updated_at->format('Y-m-d :i:s')
      ];
    }
}
