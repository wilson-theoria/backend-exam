<?php

namespace App;

use App\Comments;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
  protected $fillable = ['user_id', 'title', 'slug', 'content'];

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  public function comments()
  {
      return $this->morphMany(Comments::class, 'commentable')->whereNull('parent_id');
  }

  /**
   * Comment replies
   *
   * @return
   */
  public function replies()
  {
      return $this->hasMany(Comments::class, 'parent_id');
  }
}
