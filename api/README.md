## Laravel RESTFul API Endpoint

# Kindly copy env.sample and your config files

```php
composer install
```

# Publish Postman endpoints here
```php
https://documenter.getpostman.com/view/2387952/S17jXCcp
```

