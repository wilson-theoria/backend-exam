<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => ''], function(Router $api) {
        $api->get('posts', 'App\\Api\\V1\\Controllers\\PostController@index');
        $api->get('posts/{post}', 'App\\Api\\V1\\Controllers\\PostController@show');
        $api->get('posts/{post}/comments', 'App\\Api\\V1\\Controllers\\CommentController@index');

        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');
        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $api->get('me', 'App\\Api\\V1\\Controllers\\UserController@me');
    });

    $api->group(['middleware' => 'jwt.auth'], function ($api) {
      $api->post('posts', 'App\\Api\\V1\\Controllers\\PostController@store');
      $api->put('posts/{post}', 'App\\Api\\V1\\Controllers\\PostController@update');
      $api->delete('posts/{post}', 'App\\Api\\V1\\Controllers\\PostController@destroy');
      $api->post('posts/{post}/comments', 'App\\Api\\V1\\Controllers\\CommentController@store');
      $api->put('posts/{post}/comments/{comments}', 'App\\Api\\V1\\Controllers\\CommentController@update');
      $api->delete('posts/{post}/comments/{comments}', 'App\\Api\\V1\\Controllers\\CommentController@destroy');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });

    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });
});
